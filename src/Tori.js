import React from "react";
import "./Tori.css";

const Tori = () => {
  return (
    <div className="tori">
  <div className="search-section">
    <input type="text" placeholder="Hakusana ja/tai postinumero" />
    <select name="categories">
      <option value="all">Kaikki osastot</option>
    </select>
    <select name="locations">
      <option value="whole-finland">Koko Suomi</option>
    </select>
  </div>
  <div className="checkbox-section">
    <label><input type="checkbox" name="sell" /> Myydään</label>
    <label><input type="checkbox" name="buy" /> Ostetaan</label>
    <label><input type="checkbox" name="rent" /> Vuokrataan</label>
    <label><input type="checkbox" name="rent-wanted" /> Halutaan vuokrata</label>
    <label><input type="checkbox" name="giveaway" /> Annetaan</label>
  </div>
  <div className="button-section">
    <button type="button" className="save-search">Tallenna haku</button>
    <button type="button" className="search">Hae</button>
  </div>
</div>


    
  );
};

export default Tori;
